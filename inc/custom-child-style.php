<?php /*
$mainColor = Main Color
$seColor = Secondary Color
$terColor = tertiary Color

== Change Body typography ==

$font_body =  $fontsColor['font-family']
$fontsColor['font-color'] = Body typography Color
$fontHover = Hover color text

== Nav Stylish ==

 $navBg['font-color']
 $navBg['font-family']
 $navBg['font-size']
 $navBg['font-style']
 $navBg['font-weight']

== Nav Background ==

 $navBg['background-color']
 $navBg['background-repeat']
 $navBg['background-attachment']
 $navBg['background-position']
 $navBg['background-size']
 $navBg['background-image']

== Main Background ==

 $mainBg['background-color']
 $mainBg['background-repeat']
 $mainBg['background-attachment']
 $mainBg['background-position']
 $mainBg['background-size']
 $mainBg['background-image']


== Footer Background ==

 $footerBg['background-color']
 $footerBg['background-repeat']
 $footerBg['background-attachment']
 $footerBg['background-position']
 $footerBg['background-size']
 $footerBg['background-image']

*/?>
<style id="indohotels-style" type="text/css">
	body {
		font-family: <?php echo isset($font_body) ? $font_body : ''; ?>;
		color: <?php echo isset($fontsColor['font-color']) ? $fontsColor['font-color'] : ''; ?>;
	}

	::selection {
		background: <?php echo $mainColor; ?>;
	}

	.nav-background,
	.nav-no-image,
	.nav ul.dropdown-menu {
		background-color: <?php echo $mainColor; ?>;
	}
	nav .navbar-custom {
		background-color: <?php echo $terColor; ?>;
	}

	/* BUTTON ========================================================== */
	button,
	html input[type=button],
	input[type=reset],
	input[type=submit] {
		border: 1px solid <?php echo $mainColor; ?>;
		background-color: <?php echo $mainColor; ?>;
	}

	.btn-outline:hover,
	.btn-outline:focus,
	.btn-outline:active {
		color: <?php echo $seColor; ?>;
	}

	.btn-check
	.btn-check:active {
		background-color: <?php echo $mainColor; ?>
	}

	.btn-check:hover {
		background-color: <?php echo $seColor; ?>;
	}

	/* HEADING ========================================================== */
	h1.title {
		color: <?php echo $mainColor; ?>
	}

	.footer-distributed .footer-links li:hover a,
	a:hover {
		color: <?php echo $fontHover; ?>;

	}

	.book-room,
	.home-gallery .title-gallery-home span.line-color,
	.contact-headline {
		background-color: <?php echo $mainColor; ?>
	}

	.book-room:hover a,
	.owl-prev .glyphicon.glyphicon-chevron-left,
	.owl-next .glyphicon.glyphicon-chevron-right {
		color: <?php echo $seColor; ?>;
	}

	.booking .form-control,
	.booking .form-control[readonly],
	.booking .input-group-addon {
		border: 1px solid <?php echo $mainColor; ?>;

	}
	.box-gallery {
		border-top: 2px solid <?php echo $mainColor; ?>;
		border-bottom: 2px solid <?php echo $mainColor; ?>;
	}

	@media (max-width: 1024px){
		.booking-box {
		  position: static;
		  max-width: 100%;
			background: <?php echo $mainColor; ?>;
		}
	}

	#home-main-info span,
	.box-heading h2:after {
		background: <?php echo $mainColor; ?>;
	}

	.info-text .info-icon .icon-round {
		background: <?php echo $mainColor; ?>;
	}

	#home-main-info h2,
	.box-heading h2 {
		color: <?php echo $mainColor; ?>;
	}

	#home-main-info .box-room {
		background-color: <?php echo $terColor; ?>;
	}

	.box-home-grid .overlay {
		background-color: <?php echo $terColor; ?>;
	}
	/* Facilities */
	.box-text h4 {
		color: <?php echo $seColor; ?>;
	}
	.box h4 {
		background-color: <?php echo $mainColor; ?>;
	}
	.box-home-grid {
		background-color: <?php echo $terColor; ?>;
	}

	.slick-prev:before, .slick-next:before {
		color: <?php echo $mainColor; ?>;
	}

	/* HOMEPAGE SECTION ========================================================== */
	.hotel-info-home {
		background-image: url(<?php echo $section3bg['background-image']; ?>);
		background-repeat: <?php echo $section3bg['background-repeat']; ?>;
		background-color: <?php echo $section3bg['background-color']; ?>;
	}

	/* FOOTER ========================================================== */
	.footer-distributed {
		background-color: <?php echo $mainColor; ?>;
	}
	.footer-distributed .krs_info_widget  p a {
		color: <?php echo $seColor; ?>;
	}
	.footer-distributed .krs_info_widget i {
		background-color: <?php echo $seColor; ?>;
	}
	.footer-distributed .footer-icons a {
		background-color: <?php echo $seColor; ?>;
	}
	.footer-distributed .footer-icons a:hover {
		background-color: <?php echo $mainColor; ?>;
		color: #ffffff;
		border: 2px solid <?php echo $mainColor; ?>;
	}

	.footer {
		clear: both;
		background-color: <?php echo isset($footerBg['background-color']) ? $footerBg['background-color'] : '#6d6e71'; ?>;
		background-image: url(<?php echo isset($footerBg['background-image']) ? $footerBg['background-image'] : ''; ?>);
		background-repeat: <?php echo isset($footerBg['background-repeat']) ? $footerBg['background-repeat'] : ''; ?>;
		border-top: 2px solid <?php echo isset($mainColor) ? $mainColor : '#000'; ?>;
	}

	/* CALERAN ========================================================== */
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start:not(.caleran-hovered) span,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end:not(.caleran-hovered) span,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start,
	.caleran-container .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-selected,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-start,
	.caleran-container-mobile .caleran-input .caleran-calendars .caleran-calendar .caleran-days-container .caleran-day.caleran-end {
		background-color: <?php echo $seColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day,
	.caleran-container .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-end-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-end .caleran-header-start-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-end-day, .caleran-container-mobile .caleran-input .caleran-header .caleran-header-start .caleran-header-start-day {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container-mobile .caleran-input .caleran-header .caleran-header-separator {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-today,
	.caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-today {
		color: <?php echo $seColor; ?>;
	}
	.caleran-container-mobile .caleran-input .caleran-footer button.caleran-cancel {
		border: 1px solid <?php echo $seColor; ?> !important;
		color: <?php echo $seColor; ?> !important;
	}
	.caleran-apply-d,
	.caleran-apply {
		background: <?php echo $seColor; ?> !important;
		border: 1px solid <?php echo $seColor; ?> !important;
	}
	.caleran-container .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick,
	.caleran-container-mobile .caleran-input .caleran-calendar .caleran-days-container .caleran-day-unclick {
		background: <?php echo $seColor; ?>;
	}
</style>
