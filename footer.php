<div class="clearfix"></div>
	<footer class="footer">
		<div class="footer-center">
			<div class="container">

			<?php if(!empty(ot_get_option('krs_address'))) : ?>
			<h4 class="widget-title">ADDRESS</h4>
			<div class="footer-address">
				<i class="fa fa-map-marker"></i>
				<p><?php echo ot_get_option('krs_address'); ?></p>
			</div><!-- end .footer-address -->
			<?php endif; ?>

			<?php if(!empty(ot_get_option('krs_phone'))) : ?>
				<div class="footer-phone">
					<i class="fa fa-phone"></i>
					<p><?php echo ot_get_option('krs_phone'); ?></p>
				</div>
			<?php endif; ?>

			<?php if(!empty(ot_get_option('krs_email'))) : ?>
				<div class="footer-email">
					<i class="fa fa-envelope"></i>
					<p>
						<a href="mailto:<?php echo ot_get_option('krs_email'); ?>">
							<?php echo ot_get_option('krs_email'); ?>
						</a>
					</p>
				</div><!-- end .footer-email -->
			<?php endif; ?>
			</div><!-- end .container -->
		</div><!-- end .footer-center -->

		<?php krs_sn(); ?>
		<nav class="nav text-center nav-bottom">
			<?php karisma_nav_footer(); ?>
		</nav>

		<div class="footer-credits">
			<div class="container">
				<?php echo ot_get_option('krs_footcredits'); ?>
			</div>
		</div>
	</footer>
</div>
<!-- /wrapper -->
<?php wp_footer(); ?>
</body>
</html>
