<?php get_header('image'); ?>

<main role="main">
	<div class="container">
		<!-- section -->
		<section>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
				<p class="news-meta">by <?php the_author(); ?>, at <?php the_date(); ?></p>
				<h2 class="room-title"><?php the_title(); ?></h2>
				<?php the_content(); ?>
			</article>
			<!-- /article -->

			<?php endwhile; ?>
			<?php else: ?>

			<!-- article -->
			<article>

				<h1><?php _e( 'Sorry, nothing to display.', 'indohotels' ); ?></h1>

			</article>
			<!-- /article -->

			<?php endif; ?>

		</section>
		<!-- /section -->
	</div>
</main>

<?php get_footer(); ?>
