<?php /* Template Name: Home Page Template */  ?>
<?php get_header('home' ); ?>

<main role="main">

	<section id="home-main-info">
		<div class="container">
			<h2 class="hotel-title"><?php echo ot_get_option('krs_ros_in_title');?></h2>
			<span></span>
			<p><?php echo ot_get_option('krs_ros_in');?></p>
		</div>
	</section>

	<?php if (ot_get_option('krs_room_actived') != 'no') : ?>
	<section id="box-feature" style="background-image: url(<?php echo ot_get_option('krs_background_text1'); ?>);">
		<div class="container">

			<?php
			 $args = array(
				'post_type'=> ot_get_option('krs_section_2'),
				'posts_per_page' => 6,
			);

			$krs_query = new WP_Query( $args );
			$count = $krs_query->post_count;
			if(($count == 2) || ($count == 4)) {
				$col = 'col-md-6';
			} else {
				$col = 'col-md-4';
			}

			if ($krs_query->have_posts()): ?>

			<div class="box-bg" <?php if($col == 'col-md-4') { echo 'style="width: 100%;";'; } ?>>
				<div class="box-heading">
					<h2><?php echo ot_get_option('krs_headline_section2');?></h2>
				</div>

				<div class="row">
					<?php while ($krs_query->have_posts()) : $krs_query->the_post(); ?>
					<div class="<?php echo $col ?>">
						<div class="box-room">
							<div class="box-thumb">
								<?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_post_thumbnail('large'); // Declare pixel size you need inside the array ?>
								</a>
								<h4><?php the_title(); ?></h4>
								<?php endif; ?>
							</div>

						</div>
					</div>
					<?php endwhile; ?>
				</div>
					<?php endif; ?>
			</div> <!-- /box-bg -->
		</div><!-- end .container -->
	</section>
	<?php endif; ?>

	<div class="clearfix"></div>

	<section class="box-home-gallery">
		<div class="container">
			<div class="box-heading">
				<h2>Our Gallery</h2>
			</div>
			<div class="gallery-box">
				<?php
				$args = array(
					'post_type' => 'gallery',
					'phototype'  => 'home',
					);
				query_posts($args);
				if (have_posts()) : while (have_posts()) : the_post(); ?>
				<div class="item thumb">
					<div class="thumbnails">
						<!-- post thumbnail -->
						<?php if ( has_post_thumbnail()) : //Check if thumbnail exists ?>
							<?php the_post_thumbnail('gallery-slide'); // Declare pixel size you need inside the array ?>
						<?php endif; ?>
						<!-- /post thumbnail -->
					</div>
					<!-- post title -->
					<div class="title-gallery-home">
						<h2 class="gallery-home-title"><?php echo get_the_title(); ?></h2>
					</div>
				</div>
				<!-- /post title -->
			<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>

	<?php footer_slide(); ?>

</main>

<?php get_footer(); ?>
